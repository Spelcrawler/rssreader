package ru.spel.rssreader.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import ru.spel.rssreader.activities.AsyncActivity;

/**
 * Retain fragment for handle async operation
 */
public class RetainFragment extends Fragment {

    private TaskCallbacks mTaskCallbacks;

    public interface TaskCallbacks {
        void onPreExecute();
        void onPostExecute(int error);
        int doInBackground();
    }

    public static RetainFragment newInstance() {
        return new RetainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTaskCallbacks = (TaskCallbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTaskCallbacks = null;
    }

    public void startTask() {
        if (mTaskCallbacks != null)
            new AsyncOperation().execute();
    }

    private class AsyncOperation extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mTaskCallbacks != null)
                mTaskCallbacks.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            if (mTaskCallbacks == null) {
                return AsyncActivity.ERROR_ANY;
            } else {
                return mTaskCallbacks.doInBackground();
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (mTaskCallbacks != null)
                mTaskCallbacks.onPostExecute(integer);
        }
    }

}
