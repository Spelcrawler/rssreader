package ru.spel.rssreader.rss;

import java.io.Serializable;

/**
 * Created by Spel on 15.01.2016.
 */
public class RSSItem implements Serializable {

    private String mTitle;
    private String mContent;
    private String mUrl;
    private String mImageUrl;
    private String mAuthor;
    private Long mTime;

    private static final int CONTENT_PREVIEW_LENGTH = 100;

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public void setTime(Long time) {
        mTime = time;
    }

    public void setImageUrl(String imageUrl) {
        //Database can return string "null" and in any cases it can be null
        if (imageUrl == null || imageUrl.equals("null")) {
            mImageUrl = "";
        } else {
            mImageUrl = imageUrl;
        }
    }

    public void setAuthor(String author) {
        //Database can return string "null" and in any cases it can be null
        if (author == null || author.equals("null")) {
            mAuthor = "";
        } else {
            mAuthor = author;
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public Long getTime() {
        return mTime;
    }

    public String getContent() {
        return mContent;
    }

    public String getContentPreview() {
        if (mContent != null && mContent.length() > CONTENT_PREVIEW_LENGTH) {
            return mContent.substring(0, CONTENT_PREVIEW_LENGTH) + "...";
        } else {
            return mContent;
        }
    }

    public String getUrl() {
        return mUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getAuthor() {
        return mAuthor;
    }

}
