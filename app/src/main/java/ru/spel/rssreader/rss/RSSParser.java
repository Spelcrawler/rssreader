package ru.spel.rssreader.rss;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.xmlpull.v1.XmlPullParser.END_DOCUMENT;
import static org.xmlpull.v1.XmlPullParser.END_TAG;
import static org.xmlpull.v1.XmlPullParser.START_TAG;

/**
 * Class for parsing RSS feed to java objects
 */
public class RSSParser {

    private String mUrl;

    //Date format set with english locale as in rss specification
    private static final DateFormat pubDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    public RSSParser(String url) {
        mUrl = url;
    }

    /**
     * Parse items from url set in constructor
     *
     * @return item list, or null if errors.
     */
    public List<RSSItem> parse(){
        try {
            //Get parser
            XmlPullParser xpp = getParser(mUrl);
            if (xpp == null) {
                return null;
            }
            List<RSSItem> items = new ArrayList<>();

            while (xpp.getEventType() != END_DOCUMENT) {
                if (xpp.getEventType() == START_TAG && "item".equals(xpp.getName())) {
                    //parse item
                    items.add(parseItem(xpp));
                }

                xpp.next();
            }

            //Check items
            if (isItemsCorrect(items)) {
                return items;
            } else {
                return null;
            }
        } catch (Exception e) {
            //For all exception return null
            return null;
        }
    }

    /**
     * Parse item from xml pull parser with cursor on item tag start.
     * @param xpp xml pull parser with cursor on item tag start.
     *
     * @return item or null if incorrect content or bad connection.
     * @throws Exception
     */
    private RSSItem parseItem(XmlPullParser xpp) throws Exception {
        RSSItem item = new RSSItem();
        String imageUrl = null;
        while (!(xpp.getEventType() == END_TAG && "item".equals(xpp.getName()))) {
            if (xpp.getEventType() == START_TAG) {
                Log.d("RSS_PARSER", xpp.getName());
                switch (xpp.getName()) {
                    case "title":
                        xpp.next();
                        item.setTitle(xpp.getText());
                        break;
                    case "description":
                        xpp.next();
                        String description = xpp.getText();
                        Document descriptionDocument = Jsoup.parse(description);
                        item.setContent(descriptionDocument.text());
                        Element image = descriptionDocument.select("img").first();
                        if (image != null) {
                            item.setImageUrl(image.attr("src"));
                        }
                        break;
                    case "link":
                        xpp.next();
                        item.setUrl(xpp.getText());
                        break;
                    case "pubDate":
                        xpp.next();
                        item.setTime(pubDateFormat.parse(xpp.getText()).getTime());
                        break;
                    case "media|thumbnail":
                        if (imageUrl == null)
                            imageUrl = xpp.getAttributeValue("url", null);
                        break;
                    case "media|content":
                        if (imageUrl == null)
                            imageUrl = xpp.getAttributeValue("url", null);
                        break;
                    case "image":
                        if (imageUrl == null)
                            imageUrl = xpp.getAttributeValue("url", null);
                        break;
                    case "author":
                        xpp.next();
                        item.setAuthor(xpp.getText());
                }
            }

            xpp.next();
        }

        return item;
    }


    /**
     * Create xml pull parser with content of url
     * @param url url with rss data
     *
     * @return xml pull parser with data from url
     * @throws IOException
     * @throws XmlPullParserException
     */
    private XmlPullParser getParser(String url) throws IOException, XmlPullParserException{
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        URLConnection connection = new URL(url).openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        InputStream inputStream = connection.getInputStream();
        if (inputStream == null) {
            return null;
        }
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(inputStream, null);

        return xpp;
    }

    /**
     * Check if items correct
     * @param items item list
     * @return true - correct, false - incorrect
     */
    private boolean isItemsCorrect(List<RSSItem> items) {
        //Returns true if list not null, not empty and first item have all primary data.
        return !(items == null || items.size() == 0) && isItemCorrect(items.get(0));
    }

    /**
     * Check if item correct
     * @param item item
     * @return true - correct, false - incorrect
     */
    private boolean isItemCorrect(RSSItem item) {
        //Returns true if item have title, content, time, and url.
        return !(item.getTitle() == null || item.getContent() == null || item.getTime() == null || item.getUrl() == null);
    }


}
