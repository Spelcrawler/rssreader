package ru.spel.rssreader.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.spel.rssreader.R;
import ru.spel.rssreader.activities.ContentActivity;
import ru.spel.rssreader.rss.RSSItem;

/**
 * Adapter for main activity list
 */
public class RSSFeedAdapter extends RecyclerView.Adapter<RSSFeedAdapter.ViewHolder> {

    private List<RSSItem> mItems;

    public RSSFeedAdapter(List<RSSItem> items) {
        mItems = items;
    }

    public void setItems(List<RSSItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_rss_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final RSSItem item = mItems.get(position);

        if (item.getImageUrl().isEmpty()) {
            viewHolder.image.setVisibility(View.GONE);
        } else {
            viewHolder.image.setVisibility(View.VISIBLE);
            Glide.with(viewHolder.itemView.getContext()).load(item.getImageUrl()).into(viewHolder.image);
        }

        viewHolder.title.setText(item.getTitle());
        viewHolder.content.setText(item.getContentPreview());

        viewHolder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ContentActivity.class);
                intent.putExtra(ContentActivity.ARG_ITEM, item);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title;
        TextView content;
        View mainView;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.title);
            content = (TextView) itemView.findViewById(R.id.content);
            mainView = itemView.findViewById(R.id.mainView);
        }
    }


}
