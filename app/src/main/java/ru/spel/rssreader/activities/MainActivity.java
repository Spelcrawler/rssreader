package ru.spel.rssreader.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ru.spel.rssreader.R;
import ru.spel.rssreader.adapters.RSSFeedAdapter;
import ru.spel.rssreader.data.DBOperations;
import ru.spel.rssreader.data.PreferencesUtils;
import ru.spel.rssreader.rss.RSSItem;
import ru.spel.rssreader.rss.RSSParser;

public class MainActivity extends AsyncActivity {

    private static final String FEED_URL = "http://st.kp.yandex.net/rss/news.rss";
    private static final String RSS_ITEMS_TAG = "rss_items";

    private List<RSSItem> mRSSItems = new ArrayList<>();
    private RSSFeedAdapter mFeedAdapter;
    private DBOperations mDBOperations;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDBOperations = new DBOperations(this);
        //Try to restore rss items
        mRSSItems = mDBOperations.getRSSItems();
        //Create adapter
        mFeedAdapter = new RSSFeedAdapter(mRSSItems);
        //Setup recyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mFeedAdapter);

        //Setup swipe refresh layout
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startTask();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(RSS_ITEMS_TAG, (Serializable) mRSSItems);
    }

    @Override
    protected int backgroundTask() {
        //Get feed url from preferences
        String feedUrl = new PreferencesUtils(this).getFeedUrl();
        //Parse feed
        List<RSSItem> items = new RSSParser(feedUrl).parse();
        //Check items before save
        if (items == null) {
            return ERROR_ANY;
        }
        //Save items to database
        mDBOperations.saveRSSItems(items);
        //Get items from database
        mRSSItems = mDBOperations.getRSSItems();
        //Save and then get needs to simple items sort
        //All done return no error

        return ERROR_NONE;
    }

    @Override
    protected void onTaskStateChanged(final boolean isWorking) {
        //Swipe refresh layout do not animate before measured
        //simple way run animation after delay
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(isWorking);
            }
        }, 500);
    }

    @Override
    protected void onDone() {
        mFeedAdapter.setItems(mRSSItems);
    }

    @Override
    protected void onError(int error) {
        Toast.makeText(this, R.string.error_download, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            //Open settings activity
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
