package ru.spel.rssreader.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import ru.spel.rssreader.utils.RetainFragment;

/**
 * Created by Spel on 18.01.2016.
 */
public abstract class AsyncActivity extends BaseActivity implements RetainFragment.TaskCallbacks {

    public static final int ERROR_NONE = -1;
    public static final int ERROR_ANY = 0;

    private static final String RETAIN_FRAGMENT_TAG = "retain_fragment";
    private static final String WORK_STATUS_TAG = "work_status";

    private boolean mIsWorking = false;
    private RetainFragment mRetainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get work status
        if (savedInstanceState != null) {
            mIsWorking = savedInstanceState.getBoolean(WORK_STATUS_TAG);
        }

        //Try to find retain fragment in fragment manager
        FragmentManager manager = getSupportFragmentManager();
        mRetainFragment = (RetainFragment) manager.findFragmentByTag(RETAIN_FRAGMENT_TAG);
        //If no fragment in manager create new
        if (mRetainFragment == null) {
            mRetainFragment = RetainFragment.newInstance();
            manager.beginTransaction().add(mRetainFragment, RETAIN_FRAGMENT_TAG).commit();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //Send work status after activity created.
        onTaskStateChanged(mIsWorking);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save work status.
        outState.putBoolean(WORK_STATUS_TAG, mIsWorking);
    }

    protected void startTask() {
        mRetainFragment.startTask();
    }

    /**
     * Method for run in background
     * @return error code
     */
    protected abstract int backgroundTask();

    /**
     * Method for handle start and end of background task. Runs on UI thread.
     * Also runs on onResume().
     * @param isWorking
     */
    protected abstract void onTaskStateChanged(boolean isWorking);

    /**
     * Calls if background task done without errors.
     */
    protected abstract void onDone();

    /**
     * Calls if background task ends with error.
     * @param error error code.
     */
    protected abstract void onError(int error);

    public void onPreExecute() {
        mIsWorking = true;
        onTaskStateChanged(true);
    }

    public void onPostExecute(int error) {
        mIsWorking = false;
        onTaskStateChanged(false);
        if (error == ERROR_NONE) {
            onDone();
        } else {
            onError(error);
        }
    }

    public int doInBackground() {
        return backgroundTask();
    }


}
