package ru.spel.rssreader.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import ru.spel.rssreader.R;
import ru.spel.rssreader.data.DBOperations;
import ru.spel.rssreader.data.PreferencesUtils;
import ru.spel.rssreader.rss.RSSItem;
import ru.spel.rssreader.rss.RSSParser;

public class SettingsActivity extends AsyncActivity {

    private EditText mURLEditText;
    private Button mSaveButton;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setNeedShowBackButton(true);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mURLEditText = (EditText) findViewById(R.id.urlEditText);
        mSaveButton = (Button) findViewById(R.id.saveButton);
        mSaveButton.setOnClickListener(onSaveClicked);
    }

    @Override
    protected int backgroundTask() {
        String url = String.valueOf(mURLEditText.getText());
        List<RSSItem> items = new RSSParser(url).parse();
        if (items == null) {
            return MainActivity.ERROR_ANY;
        }
        DBOperations dbOperations = new DBOperations(this);
        dbOperations.removeRSSItems();
        dbOperations.saveRSSItems(items);
        new PreferencesUtils(this).setFeedUrl(String.valueOf(mURLEditText.getText()));

        return MainActivity.ERROR_NONE;
    }

    @Override
    protected void onTaskStateChanged(boolean isWorking) {
        mURLEditText.setEnabled(!isWorking);
        mSaveButton.setEnabled(!isWorking);
        mProgressBar.setVisibility(isWorking ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onDone() {
        Toast.makeText(this, R.string.message_url_saved, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onError(int error) {
        Toast.makeText(this, R.string.error_incorrect_url, Toast.LENGTH_SHORT).show();
    }

    View.OnClickListener onSaveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startTask();
        }
    };


}
