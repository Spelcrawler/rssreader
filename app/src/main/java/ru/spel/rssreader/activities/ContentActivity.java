package ru.spel.rssreader.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.Date;

import ru.spel.rssreader.R;
import ru.spel.rssreader.rss.RSSItem;

public class ContentActivity extends BaseActivity {

    public static final String ARG_ITEM = "item";
    private RSSItem mRSSItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        //show back button
        setNeedShowBackButton(true);

        //Get item from intent
        mRSSItem = (RSSItem) getIntent().getSerializableExtra(ARG_ITEM);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(mRSSItem.getTitle());
        title.setOnClickListener(titleClickListener);

        ((TextView) findViewById(R.id.content)).setText(mRSSItem.getContent());

        //Format date with current locale
        String date = DateUtils.formatDateTime(this, mRSSItem.getTime(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
        ((TextView) findViewById(R.id.time)).setText(date);

        //Set author name, or hide text view
        TextView authorTextView = (TextView) findViewById(R.id.author);
        if (mRSSItem.getAuthor().isEmpty()) {
            authorTextView.setText(mRSSItem.getAuthor());
        } else {
            authorTextView.setVisibility(View.GONE);
        }

        //Set image or hide image view.
        ImageView image = (ImageView) findViewById(R.id.image);
        if (mRSSItem.getImageUrl().isEmpty()) {
            image.setVisibility(View.GONE);
        } else {
            Glide.with(this).load(mRSSItem.getImageUrl()).into(image);
        }
    }

    View.OnClickListener titleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Show content in web view
            Intent intent = new Intent(view.getContext(), WebViewActivity.class);
            intent.putExtra(WebViewActivity.ARG_CONTENT_URL, mRSSItem.getUrl());
            startActivity(intent);
        }
    };

}
