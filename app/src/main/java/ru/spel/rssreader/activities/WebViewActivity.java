package ru.spel.rssreader.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.spel.rssreader.R;

public class WebViewActivity extends BaseActivity {

    public static final String ARG_CONTENT_URL = "content_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        setNeedShowBackButton(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String url = getIntent().getStringExtra(ARG_CONTENT_URL);
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                //Handle redirect for open in webView
                view.loadUrl(url);
                return false;
            }
        });
        webView.loadUrl(url);
    }
}
