package ru.spel.rssreader.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class for store shared preferences
 */
public class PreferencesUtils {

    private static final String PREFERENCES_NAME = "preferences";
    private static final String TAG_FEED_URL = "feed_url";
    private static final String DEFAULT_FEED_URL = "http://st.kp.yandex.net/rss/news.rss";

    private SharedPreferences mPreferences;

    public PreferencesUtils(Context context) {
        mPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void setFeedUrl(String url) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(TAG_FEED_URL, url);
        editor.apply();
    }

    public String getFeedUrl() {
        return mPreferences.getString(TAG_FEED_URL, DEFAULT_FEED_URL);
    }

}
