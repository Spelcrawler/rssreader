package ru.spel.rssreader.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Spel on 18.01.2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "database.db";
    private static final int DATABASE_VERSION = 1;


    public static final String RSS_ITEMS_TABLE_NAME = "rss_item";
    public static final String RSS_ITEMS_TITLE = "title";
    public static final String RSS_ITEMS_CONTENT = "content";
    public static final String RSS_ITEMS_TIME = "time";
    public static final String RSS_ITEMS_URL = "url";
    public static final String RSS_ITEMS_AUTHOR = "author";
    public static final String RSS_ITEMS_IMAGE_URL = "image_url";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + RSS_ITEMS_TABLE_NAME + " (" +
                RSS_ITEMS_TITLE + " TEXT NOT NULL, " +
                RSS_ITEMS_CONTENT + " TEXT NOT NULL, " +
                RSS_ITEMS_TIME + " INTEGER NOT NULL UNIQUE, " +
                RSS_ITEMS_URL + " TEXT, " +
                RSS_ITEMS_AUTHOR + " TEXT, " +
                RSS_ITEMS_IMAGE_URL + " TEXT);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int fromVetsion, int toVersion) {

    }
}
