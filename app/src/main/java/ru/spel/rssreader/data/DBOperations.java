package ru.spel.rssreader.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ru.spel.rssreader.rss.RSSItem;

import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_AUTHOR;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_CONTENT;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_IMAGE_URL;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_TABLE_NAME;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_TIME;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_TITLE;
import static ru.spel.rssreader.data.DBHelper.RSS_ITEMS_URL;

/**
 * Created by Spel on 18.01.2016.
 */
public class DBOperations {

    private DBHelper mDB;

    public DBOperations(Context context) {
        mDB = new DBHelper(context);
    }

    /**
     * Save item list to database
     * @param items items to save
     */
    public void saveRSSItems(List<RSSItem> items) {
        SQLiteDatabase db = mDB.getWritableDatabase();
        db.beginTransaction();
        for (RSSItem item : items) {
            saveRSSItem(db, item);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    /**
     * Get items from database
     * @return all items from database sorted by date
     */
    public List<RSSItem> getRSSItems() {
        SQLiteDatabase db = mDB.getReadableDatabase();
        List<RSSItem> items = getRSSItems(db);
        db.close();

        return items;
    }

    /**
     * Delete all items from database
     */
    public void removeRSSItems() {
        SQLiteDatabase db = mDB.getWritableDatabase();
        db.execSQL("DELETE FROM " + RSS_ITEMS_TABLE_NAME);
        db.close();
    }

    /**
     * Save item to database
     * @param db opened database
     * @param item item to save
     */
    private void saveRSSItem(SQLiteDatabase db, RSSItem item) {
        String query = String.format("INSERT OR REPLACE INTO %s (%s, %s, %s, %s, %s, %s) VALUES ('%s', '%s', '%s', %s, '%s', '%s')", RSS_ITEMS_TABLE_NAME,
                RSS_ITEMS_TITLE, RSS_ITEMS_CONTENT, RSS_ITEMS_URL, RSS_ITEMS_TIME, RSS_ITEMS_AUTHOR, RSS_ITEMS_IMAGE_URL,
                item.getTitle(), item.getContent(), item.getUrl(), item.getTime(), item.getAuthor(), item.getImageUrl());
        db.execSQL(query);
    }

    /**
     * Get items from database
     * @param db opened database
     * @return all items from database
     */
    private List<RSSItem> getRSSItems(SQLiteDatabase db) {
        List<RSSItem> items = new ArrayList<>();
        String query = String.format("SELECT * FROM %s ORDER BY %s DESC", RSS_ITEMS_TABLE_NAME, RSS_ITEMS_TIME);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            items.add(getItemFromCursor(cursor));
        }
        cursor.close();

        return items;
    }

    /**
     * Get item from cursor
     * @param cursor cursor
     * @return item
     */
    private RSSItem getItemFromCursor(Cursor cursor) {
        RSSItem item = new RSSItem();
        item.setTitle(cursor.getString(cursor.getColumnIndex(RSS_ITEMS_TITLE)));
        item.setContent(cursor.getString(cursor.getColumnIndex(RSS_ITEMS_CONTENT)));
        item.setUrl(cursor.getString(cursor.getColumnIndex(RSS_ITEMS_URL)));
        item.setTime(cursor.getLong(cursor.getColumnIndex(RSS_ITEMS_TIME)));
        item.setAuthor(cursor.getString(cursor.getColumnIndex(RSS_ITEMS_AUTHOR)));
        item.setImageUrl(cursor.getString(cursor.getColumnIndex(RSS_ITEMS_IMAGE_URL)));

        return item;
    }


}
